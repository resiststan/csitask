package net.resiststan;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PriceUtilsImpl implements PriceUtils {
    @Override
    public List<Price> updatePriceList(List<Price> oldPriceList, List<Price> newPriceList) {
        ArrayList<Price> result = new ArrayList<>();

        Map<ProductNumberDepart, List<Price>> groupedOldPrices = groupByProductDepartNumber(oldPriceList);
        Map<ProductNumberDepart, List<Price>> groupedNewPrices = groupByProductDepartNumber(newPriceList);

        List<ProductNumberDepart> productNumbersOldWithOutChangeAndNewDepart = groupedOldPrices.keySet().stream()
                .filter(key -> !groupedNewPrices.containsKey(key))
                .toList();

        for (ProductNumberDepart pnd : productNumbersOldWithOutChangeAndNewDepart) {
            List<Price> prices = groupedOldPrices.get(pnd);
            result.addAll(prices != null ? prices : groupedNewPrices.get(pnd));
        }

        List<ProductNumberDepart> productNumbersForUpdateDepart = groupedOldPrices.keySet().stream()
                .filter(groupedNewPrices::containsKey)
                .toList();

        for (ProductNumberDepart pnd : productNumbersForUpdateDepart) {
            result.addAll(joinPriceLists(groupedOldPrices.get(pnd), groupedNewPrices.get(pnd), pnd));
        }

        return result;
    }

    private List<Price> joinPriceLists(List<Price> oldPriceList, List<Price> newPriceList, ProductNumberDepart pnd) {
        List<Price> result = new ArrayList<>();

        List<PriceDate> priceDates = convertToPriceDateList(oldPriceList, newPriceList);

        int i = 0;
        do {
            PriceDate p1 = priceDates.get(i);
            int priority = p1.priority;

            if (priceDates.size() == i + 1) {
                PriceDate p2 = priceDates.get(i - 1);
                result.add(createPrice(pnd, p2.date, p1.date, p1.value));
                break;
            }

            for (int j = i + 1; j < priceDates.size(); j++) {
                PriceDate p2 = priceDates.get(j);

                if (p1.dateType.isBegin()) {
                    priority = Math.max(priority, p2.priority);

                    if (p2.value == p1.value) {
                        if (p2.dateType.isEnd() && p2.priority == priority) {
                            result.add(createPrice(pnd, p1.date, p2.date, p1.value));
                            i = j + 1;
                            break;
                        }
                    } else {
                        if (p2.priority >= priority) {
//                            result.add(createPrice(pnd, p1.date, p2.date.minus(1, ChronoUnit.SECONDS), p1.value));
                            result.add(createPrice(pnd, p1.date, p2.date, p1.value));
                            i = j;
                            break;
                        }
                    }
                } else {
                    if (p1.dateType.isEnd()) {
                        result.add(createPrice(pnd, priceDates.get(i - 1).date, p1.date, p1.value));
                        i++;
                        break;
                    }
                }
            }
        } while (i < priceDates.size());

        return result;
    }

    private Price createPrice(ProductNumberDepart pnd, LocalDateTime begin, LocalDateTime end, long value) {
        return new Price(pnd.productCode, pnd.number, pnd.depart, begin, end, value);
    }

    private List<PriceDate> convertToPriceDateList(List<Price> oldPriceList, List<Price> newPriceList) {
        return Stream.concat(toPojoStream(oldPriceList, 0), toPojoStream(newPriceList, 1))
                .flatMap(Collection::stream)
                .sorted(Comparator.comparing(PriceDate::date))
                .toList();
    }

    private Stream<List<PriceDate>> toPojoStream(List<Price> priceList, int priority) {
        return priceList.stream()
                .map(x -> Arrays.asList(
                        new PriceDate(x.getBegin(), DateType.BEGIN, x.getValue(), priority),
                        new PriceDate(x.getEnd(), DateType.END, x.getValue(), priority))
                );
    }

    private Map<ProductNumberDepart, List<Price>> groupByProductDepartNumber(List<Price> priceList) {
        return priceList.stream()
                .collect(Collectors.groupingBy(p -> new ProductNumberDepart(p.getProductCode(), p.getDepart(), p.getNumber())));
    }

    private record ProductNumberDepart(String productCode, int depart, int number) {
    }

    private record PriceDate(LocalDateTime date, DateType dateType, long value, int priority) {
    }

    private enum DateType {
        BEGIN,
        END;

        boolean isBegin() {
            return this.equals(BEGIN);
        }

        boolean isEnd() {
            return this.equals(END);
        }
    }
}
