package net.resiststan;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Price {
//    private long id; // идентификатор в БД
    private String productCode; // код товара
    private int number; // номер цены
    private int depart; // номер отдела
    private LocalDateTime begin; // начало действия
    private LocalDateTime end; // конец действия
    private long value; // значение цены в копейках
}
