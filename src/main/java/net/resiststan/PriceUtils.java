package net.resiststan;

import java.util.List;

public interface PriceUtils {
    List<Price> updatePriceList(List<Price> oldPriceList, List<Price> newPriceList);
}
