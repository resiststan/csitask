package net.resiststan;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PriceUtilsImplTest {

    private static PriceUtils priceUtils;

    @BeforeAll
    static void beforeAll() {
        priceUtils = new PriceUtilsImpl();
    }

    @ParameterizedTest(name = "case{index}")
    @ArgumentsSource(EmployeesArgumentsProvider.class)
    void update(List<Price> pricesOld, List<Price> pricesNew, List<Price> expectResult) {
        List<Price> actualResult = priceUtils.updatePriceList(pricesOld, pricesNew);

        assertEquals(expectResult.size(), actualResult.size(), "Expected and actual size is not equals");
        assertThat(actualResult)
                .overridingErrorMessage(
                        "Expected list to contain: \n%s \nbut it contained: \n%s",
                        Arrays.toString(expectResult.toArray()),
                        Arrays.toString(actualResult.toArray())
                )
                .containsAll(expectResult);
    }

    private static LocalDateTime dateTimeParser(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        return LocalDateTime.parse(str, formatter);
    }

    private static class EmployeesArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(
                    case1(),
                    case2(),
                    case3(),
                    case4(),
                    case5()
            );
        }
    }

    private static Arguments case1() {
        return Arguments.of(
                List.of(
                        new Price(
                                "122856",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("31.01.2013 23:59:59"),
                                11000
                        ),
                        new Price(
                                "122856",
                                2,
                                1,
                                dateTimeParser("10.01.2013 00:00:00"),
                                dateTimeParser("20.01.2013 23:59:59"),
                                99000
                        ),
                        new Price(
                                "6654",
                                1,
                                2,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("31.01.2013 00:00:00"),
                                5000
                        )
                ),

                List.of(
                        new Price(
                                "122856",
                                1,
                                1,
                                dateTimeParser("20.01.2013 00:00:00"),
                                dateTimeParser("20.02.2013 23:59:59"),
                                11000
                        ),
                        new Price(
                                "122856",
                                2,
                                1,
                                dateTimeParser("15.01.2013 00:00:00"),
                                dateTimeParser("25.01.2013 23:59:59"),
                                92000
                        ),
                        new Price(
                                "6654",
                                1,
                                2,
                                dateTimeParser("12.01.2013 00:00:00"),
                                dateTimeParser("13.01.2013 00:00:00"),
                                4000
                        )
                ),

                List.of(
                        new Price(
                                "122856",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("20.02.2013 23:59:59"),
                                11000
                        ),
                        new Price(
                                "122856",
                                2,
                                1,
                                dateTimeParser("10.01.2013 00:00:00"),
                                dateTimeParser("15.01.2013 00:00:00"),
                                99000
                        ),
                        new Price(
                                "122856",
                                2,
                                1,
                                dateTimeParser("15.01.2013 00:00:00"),
                                dateTimeParser("25.01.2013 23:59:59"),
                                92000
                        ),
                        new Price(
                                "6654",
                                1,
                                2,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("12.01.2013 00:00:00"),
                                5000
                        ),
                        new Price(
                                "6654",
                                1,
                                2,
                                dateTimeParser("12.01.2013 00:00:00"),
                                dateTimeParser("13.01.2013 00:00:00"),
                                4000
                        ),
                        new Price(
                                "6654",
                                1,
                                2,
                                dateTimeParser("13.01.2013 00:00:00"),
                                dateTimeParser("31.01.2013 00:00:00"),
                                5000
                        )
                )
        );
    }

    private static Arguments case2() {
        return Arguments.of(
                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("31.03.2013 00:00:00"),
                                9000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.02.2013 00:00:00"),
                                dateTimeParser("01.03.2013 00:00:00"),
                                5000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("01.02.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.02.2013 00:00:00"),
                                dateTimeParser("01.03.2013 00:00:00"),
                                5000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.03.2013 00:00:00"),
                                dateTimeParser("31.03.2013 00:00:00"),
                                9000
                        )
                )
        );
    }

    private static Arguments case3() {
        return Arguments.of(
                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("31.03.2013 23:59:59"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.04.2013 00:00:00"),
                                dateTimeParser("01.05.2013 00:00:00"),
                                8000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.03.2013 00:00:00"),
                                dateTimeParser("01.04.2013 00:00:00"),
                                5000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("01.03.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.03.2013 00:00:00"),
                                dateTimeParser("01.04.2013 00:00:00"),
                                5000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.04.2013 00:00:00"),
                                dateTimeParser("01.05.2013 00:00:00"),
                                8000
                        )
                )
        );
    }

    private static Arguments case4() {
        return Arguments.of(
                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("01.02.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.02.2013 00:00:00"),
                                dateTimeParser("01.03.2013 00:00:00"),
                                8000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.03.2013 00:00:00"),
                                dateTimeParser("01.04.2013 00:00:00"),
                                7000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.01.2013 00:00:00"),
                                dateTimeParser("15.02.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.02.2013 00:00:00"),
                                dateTimeParser("15.03.2013 00:00:00"),
                                6000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("15.02.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.02.2013 00:00:00"),
                                dateTimeParser("15.03.2013 00:00:00"),
                                6000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.03.2013 00:00:00"),
                                dateTimeParser("01.04.2013 00:00:00"),
                                7000
                        )
                )
        );
    }

    private static Arguments case5() {
        return Arguments.of(
                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("01.02.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.02.2013 00:00:00"),
                                dateTimeParser("01.03.2013 00:00:00"),
                                8000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.03.2013 00:00:00"),
                                dateTimeParser("01.04.2013 00:00:00"),
                                7000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.04.2013 00:00:00"),
                                dateTimeParser("01.05.2013 00:00:00"),
                                8000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.01.2013 00:00:00"),
                                dateTimeParser("15.02.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.02.2013 00:00:00"),
                                dateTimeParser("15.03.2013 00:00:00"),
                                6000
                        )
                ),

                List.of(
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.01.2013 00:00:00"),
                                dateTimeParser("15.02.2013 00:00:00"),
                                9000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.02.2013 00:00:00"),
                                dateTimeParser("15.03.2013 00:00:00"),
                                6000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("15.03.2013 00:00:00"),
                                dateTimeParser("01.04.2013 00:00:00"),
                                7000
                        ),
                        new Price(
                                "1",
                                1,
                                1,
                                dateTimeParser("01.04.2013 00:00:00"),
                                dateTimeParser("01.05.2013 00:00:00"),
                                8000
                        )
                )
        );
    }
}